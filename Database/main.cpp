#include "database.h"

int main() {
	char * res = 0;//db result from query
	std::string query = "SELECT names FROM test";
	database* o2 = new database("test.db");
	o2->conn();
	o2->set_query(query);
	//available options: first, last, all, random
	res = o2->run_query("random");

	printf("output %s \r\n", res);//testing print out
	o2->~database();
	delete o2;
}