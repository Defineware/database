/*
Usage Call:
	#include "database.h" // in file that will call the below

	char * res = 0;//db result from query
	std::string query = "SELECT names FROM test";

	database* o2 = new database("test.db");
	o2->conn();
	o2->set_query(query);
	//available options: first, last, all, random
	res = o2->run_query("random");

	printf("output %s \r\n", res);//testing print out
	o2->~database();
	delete o2;

Note:
	test.db is not within this repo and if ran, will be created automatically and stored within the environment type when built (debug, production)
	Results return have only been set up to allow 1 column result to be returned and not the row or multiple column results
*/

#ifndef DATABASE_H
#define DATABASE_H

#include "sqlite3.h"
#include <string>



class database {
public:
	database(char *database_name);
	~database();
	int conn();
	int get_error() { return conn(); };
	void set_query(std::string query);
	char * run_query(std::string mode);//available options : first, last, all, random
private:
	sqlite3 *db;
	sqlite3_stmt *res;
	const char *errMSG;
	const char *tail;
	const char *database_name;
	std::string query = "";
	int error = 99;
private:
	int prepare_query();
	char * return_query();//console print out the whole query, return will be corrupted
	char * return_first();//returns the first result
	char * return_last();//returns the last result
	char * return_random();//returns an random result based on the amount of results found
};

#endif
