#include "database.h"


database::database(char *dbname) {
	//set the varibles such as the database name
	database_name = dbname;
}
	

int database::conn()
{
	error = sqlite3_open(database_name, &db);
	if (error) {
		printf("Could not open DB \r\n");
		sqlite3_close(db);
		system("pause");
		return -1;
	}
	return error;

}

//** NOT USED **//
char *database::return_query() {
	int colcount = sqlite3_column_count(res);
	char *result = 0;
	while (sqlite3_step(res) == SQLITE_ROW) {
		for (int i = 0; i < colcount; i++) {
			printf("result: %s \r\n", sqlite3_column_text(res, i));
		}
	}
	sqlite3_finalize(res);
	return result;
}

char *database::return_first() {
	int colcount = sqlite3_column_count(res);
	char *result = 0;
	while (sqlite3_step(res) == SQLITE_ROW) {
		for (int i = 0; i < 1; i++) {
			result = _strdup((const char*)sqlite3_column_text(res, i));
			sqlite3_finalize(res);
			return result;
		}
	}
	
}

char *database::return_random() {
	int colcount = sqlite3_column_count(res);
	char *result = 0;
	int counter = 0;
	//loop through to get the number of rows then reset the loop
	while (sqlite3_step(res) == SQLITE_ROW) {	
		counter++;
	}
	if (counter > 0) {
		sqlite3_reset(res);

		int ran = rand() % counter + 1;
		int looper = 1;
		while (sqlite3_step(res) == SQLITE_ROW) {
			if (ran == looper) {
				for (int i = 0; i < 1; i++) {
					result = _strdup((const char*)sqlite3_column_text(res, i));
					sqlite3_finalize(res);
					return result;
				}
			}
			looper++;
		}
	}
	return "No results found!\r\n";
}

char *database::return_last() {
	int colcount = sqlite3_column_count(res);
	char *result = 0;
	while (sqlite3_step(res) == SQLITE_ROW) {
		for (int i = 0; i < colcount; i++) {
			result = _strdup((const char*)sqlite3_column_text(res, i));
		}
	}
	sqlite3_finalize(res);
	return result;
}

void database::set_query(std::string q)
{
	query = q;
}

int database::prepare_query() {
	error = sqlite3_prepare_v2(db, query.c_str(), query.length(), &res, &tail);

	if (error != SQLITE_OK) {
		printf("Could not prepare sql \r\n");
		sqlite3_close(db);
		system("pause");
		return -1;
	}

	return 0;
}

char *database::run_query(std::string mode)
{
	int err = prepare_query();
	//If the checks haven't failed ie: equal to zero run through the query statement results
	if (!err) {
		if (mode == "first") {
			return return_first();
		}else if (mode == "last") {
			return return_last();
		}
		else if (mode == "all") {
			return return_query();
		}
		else if (mode == "random") {
			return return_random();
		}
		else {
			return return_query();
		}
	}
}

database::~database()
{
	sqlite3_close(db);
}
