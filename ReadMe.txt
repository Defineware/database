Overview:-
Simple database connection for an sqlite database.
Have created structure as an object to be able to call on as required and to return multiple ways for the result to be returned.
Have however restricted the return data to only be 1 value, which queries will have to be sanitised beforehand otherwise unexpected results may occur

Requirements:-
This project has been built via Visual Studio Community 2015
Can be built in either x86 or x64 framework

*Dependencies:-
sqlite3
https://www.sqlite.org/download.html (build version: sqlite-amalgamation-3150200)

Usage Call:
	#include "database.h" // in file that will call the below

	char * res = 0;//db result from query
	std::string query = "SELECT names FROM test";

	database* o2 = new database("test.db");
	o2->conn();
	o2->set_query(query);
	//available options: first, last, all, random
	res = o2->run_query("random");

	printf("output %s \r\n", res);//testing print out
	o2->~database();
	delete o2;

Note:
	test.db is not within this repo and if ran, will be created automatically and stored within the environment type when built (debug, production)
	Results return have only been set up to allow 1 column result to be returned and not the row or multiple column results
	*sqlite3 files required have been added to this repo

Use as own risk as I do not guarantee any code provided thoughout